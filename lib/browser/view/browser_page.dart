import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:gemini_client/gemini_client.dart';
import 'package:gemtext/gemtext.dart' show Gemtext;
import 'package:url_launcher/url_launcher.dart';
import 'package:vostok/browser/view/error.dart';
import 'package:vostok/browser/view/gemtext_viewer.dart';
import 'package:vostok/browser/view/input.dart';

final uriRegex = RegExp(
  r"^([A-Za-z0-9]+:(\/\/)?)?(([A-Za-z0-9\-:]+[@\.])*([A-Za-z0-9\-]+\.[A-Za-z0-9\-]+|localhost)(:?\d+)?\/?).*$",
);
final schemeRegex = RegExp(
  r"^[A-Za-z0-9]+:(\/\/)?",
);

Uri _parse(String input) {
  Uri uri;

  if (uriRegex.hasMatch(input.trim())) {
    if (schemeRegex.hasMatch(input)) {
      uri = Uri.parse(input);
    } else {
      uri = Uri.parse("gemini://$input");
    }
  } else {
    uri = Uri.parse(
      "gemini://geminispace.info/search?${Uri.encodeComponent(input)}",
    );
  }

  if (uri.hasEmptyPath) {
    uri = uri.replace(path: "/");
  }

  return uri;
}

Uri _prepareUrl(Uri url, Uri newUrl) {
  if (newUrl.isAbsolute) return newUrl;

  Uri res;

  if (newUrl.hasAbsolutePath) {
    res = url.replace(path: newUrl.path, query: newUrl.query);
  } else {
    res = url.replace(path: url.path + newUrl.path, query: newUrl.query);
  }

  // a workaround to remove trailing question mark
  final strRes = res.toString();
  if (strRes.endsWith("?")) {
    res = Uri.parse(strRes.substring(0, strRes.length - 1));
  }

  return res;
}

class BrowserPage extends StatelessWidget {
  final String input;

  const BrowserPage(this.input, {super.key});

  @override
  Widget build(BuildContext context) {
    final url = _parse(input);

    return Scaffold(
      appBar: AppBar(title: Text("$url", overflow: TextOverflow.fade)),
      body: FutureBuilder<Response>(
        future: GeminiClient().send(host: url.host, request: Request(url: url)),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final data = snapshot.data!;

            if (data.statusCode >= 10 && data.statusCode <= 19) {
              return Input(
                prompt: data.meta,
                onSubmit: (value) => _handleInput(url, value, context),
              );
            }

            if (data.statusCode >= 40 && data.statusCode <= 69) {
              String title;
              if (data.statusCode <= 49) {
                title = "Temporary failure";
              } else if (data.statusCode <= 59) {
                title = "Permanent failure";
              } else {
                title = "Client certificate required";
              }

              return ErrorView(
                title: title,
                additionalInfo: data.meta.isEmpty ? null : data.meta,
              );
            }

            return GemtextViewer(
              Gemtext.parse(utf8.decode(data.body)),
              onUrlClick: (newUrl) => _handleLink(url, newUrl, context),
            );
          } else if (snapshot.hasError) {
            return ErrorView(
              title: "Unexpected error",
              additionalInfo: snapshot.error.toString(),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  void _handleInput(Uri url, String prompt, BuildContext context) {
    final res = url.replace(query: Uri.encodeComponent(prompt));

    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (context) => BrowserPage(res.toString()),
      ),
    );
  }

  void _handleLink(Uri url, Uri newUrl, BuildContext context) {
    final res = _prepareUrl(url, newUrl);

    if (res.isScheme("gemini")) {
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (context) => BrowserPage(res.toString()),
        ),
      );
    } else {
      launchUrl(newUrl);
    }
  }
}
