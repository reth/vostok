import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:gemtext/gemtext.dart' as gmi;

class GemtextViewer extends StatelessWidget {
  final gmi.Gemtext document;
  final void Function(Uri url)? onUrlClick;

  GemtextViewer(gmi.Gemtext document, {this.onUrlClick, Key? key})
      : document = _optimize(document),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final nodes = <Widget>[];
    for (final node in document.nodes) {
      Widget? widget;

      if (node is gmi.Text) {
        widget = TextBlock(node);
      } else if (node is gmi.Heading) {
        widget = HeadingBlock(node);
      } else if (node is gmi.ListItem) {
        widget = ListItemBlock(node);
      } else if (node is gmi.Quote) {
        widget = QuoteBlock(node);
      } else if (node is gmi.Link) {
        widget = LinkBlock(node, onUrlClick);
      } else if (node is gmi.Preformatted) {
        widget = PreformattedBlock(node);
      }

      if (widget != null) {
        nodes.add(Align(alignment: Alignment.centerLeft, child: widget));
      }
    }

    final width = MediaQuery.of(context).size.width;

    final padding = width < 960.0
        ? const EdgeInsets.symmetric(horizontal: 12.0)
        : EdgeInsets.symmetric(horizontal: 12.0 + ((width - 960.0) / 2));

    return SelectableRegion(
      focusNode: FocusNode(),
      selectionControls: materialTextSelectionControls,
      child: ListView(
        padding: padding,
        children: nodes,
      ),
    );
  }

  /// merge neighbor text nodes into one
  static gmi.Gemtext _optimize(gmi.Gemtext doc) {
    final buffer = [];
    final res = gmi.Gemtext();

    for (int i = 0; i < doc.nodes.length; i++) {
      if (doc.nodes[i] is gmi.Text) {
        while (i < doc.nodes.length) {
          if (doc.nodes[i] is gmi.Text) {
            buffer.add((doc.nodes[i] as gmi.Text).text);
          } else {
            res.nodes.add(gmi.Text(buffer.join("\n")));
            res.nodes.add(doc.nodes[i]);
            buffer.clear();
            break;
          }
          i++;
        }
        if (buffer.isNotEmpty) {
          res.push(gmi.Text(buffer.join("\n")));
          buffer.clear();
        }
      } else {
        res.nodes.add(doc.nodes[i]);
      }
    }

    return res;
  }
}

class TextBlock extends StatelessWidget {
  final gmi.Text text;
  const TextBlock(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text.text);
  }
}

class HeadingBlock extends StatelessWidget {
  final gmi.Heading heading;
  const HeadingBlock(this.heading, {Key? key}) : super(key: key);

  TextStyle? sizeToStyle(BuildContext context) {
    switch (heading.level) {
      case gmi.HeadingLevel.h1:
        return Theme.of(context).textTheme.headlineLarge;
      case gmi.HeadingLevel.h2:
        return Theme.of(context).textTheme.headlineMedium;
      case gmi.HeadingLevel.h3:
        return Theme.of(context).textTheme.headlineSmall;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      heading.body,
      style: sizeToStyle(context),
    );
  }
}

class ListItemBlock extends StatelessWidget {
  static const bullet = "\u2022";

  final gmi.ListItem item;
  const ListItemBlock(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text("$bullet ${item.body}");
  }
}

class QuoteBlock extends StatelessWidget {
  final gmi.Quote quote;
  const QuoteBlock(this.quote, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
            color: Theme.of(context).colorScheme.secondary,
            width: 2.0,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 12),
        child: Text(
          quote.body,
          style: TextStyle(color: Theme.of(context).colorScheme.secondary),
        ),
      ),
    );
  }
}

class LinkBlock extends StatelessWidget {
  final gmi.Link link;
  final void Function(Uri url)? onUrlClick;
  const LinkBlock(this.link, this.onUrlClick, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        text: link.name ?? link.to.toString(),
        style: Theme.of(context)
            .textTheme
            .labelLarge
            ?.copyWith(color: Theme.of(context).colorScheme.primary),
        recognizer: TapGestureRecognizer()
          ..onTap = () {
            if (onUrlClick != null) {
              onUrlClick!(link.to);
            }
          },
      ),
    );
  }
}

class PreformattedBlock extends StatelessWidget {
  final gmi.Preformatted pre;
  const PreformattedBlock(this.pre, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = ScrollController();

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.onBackground.withOpacity(0.1),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Scrollbar(
        controller: controller,
        child: SingleChildScrollView(
          controller: controller,
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              pre.text,
              style: Theme.of(context).textTheme.bodySmall?.copyWith(
                fontFeatures: const [FontFeature.tabularFigures()],
                fontFamily: "RobotoMono",
                height: 0.0,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
